{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE KindSignatures      #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Polynoms where

import           Data.List
import           Data.Ord
import           Data.Proxy
import           GHC.TypeLits

newtype FiniteField (n :: Nat) = FiniteField Integer
  deriving Eq

instance Show (FiniteField n) where
  show (FiniteField k) = show k

instance KnownNat n => Num (FiniteField n) where
  (FiniteField a) + (FiniteField b) =
    FiniteField $ (a + b) `mod` natVal (Proxy :: Proxy n)
  (FiniteField a) * (FiniteField b) =
    FiniteField $ (a * b) `mod` natVal (Proxy :: Proxy n)
  (FiniteField a) - (FiniteField b) =
    let n' = natVal (Proxy :: Proxy n)
    in FiniteField $ (a + n' - b) `mod` n'
  signum (FiniteField a) = FiniteField $ signum a * signum a
  abs (FiniteField a) = FiniteField $ abs a
  fromInteger a = FiniteField $ a `mod` natVal (Proxy :: Proxy n)

newtype Polynom field = Polynom [field]
  deriving Eq

toIndexed :: (Num a, Eq a) => [a] -> [(Integer, a)]
toIndexed ls = go 0 ls
  where go n (x : xs) =
          if x == fromInteger 0
          then go (n + 1) xs
          else (n, x) : go (n + 1) xs
        go _ [] = []

fromIndexed :: Num a => [(Integer, a)] -> [a]
fromIndexed ls = go 0 ls
  where go n ((i, x) : xs) =
          if n == i
          then x : go (n + 1) xs
          else 0 : go (n + 1) ((i, x) : xs)
        go _ [] = []

tidyIndexed :: Num a => [(Integer, a)] -> [(Integer, a)]
tidyIndexed = map (foldl mrg (0, fromInteger 0)) . groupBy eqFst . sortBy cmpFst
  where mrg (_, a) (i, b) = (i, a + b)
        cmpFst = comparing fst
        eqFst a b = cmpFst a b == EQ

instance (Num field, Eq field) => Num (Polynom field) where
  fromInteger n = Polynom [fromInteger n]
  Polynom a + Polynom b = Polynom $ zipWith (+) a b
  Polynom a * Polynom b =
    let mulIndexed (p, a) (q, b) = (p + q, a * b)
        a' = toIndexed a
        b' = toIndexed b
    in Polynom . fromIndexed . tidyIndexed . concat $
       map (flip map b' . mulIndexed) a'

  negate (Polynom a) = Polynom $ map negate a
  abs (Polynom a) = Polynom $ map abs a
  signum (Polynom a) = Polynom $ map signum a

instance (Num field, Eq field, Show field) => Show (Polynom field) where
  show (Polynom xs) = intercalate " + " . map mkX $ toIndexed xs
    where mkX (p, a) =
            (if a == 1 then "" else show a) ++
            (if p == 0 then "" else "x^" ++ show p)

newtype FinitePolynom (n :: Nat) field = FinitePolynom [field]
  deriving Eq

truncatePolynom
  :: (KnownNat n, Num field, Eq field)
  => Proxy n -> Polynom field -> FinitePolynom n field
truncatePolynom proxy (Polynom xs) =
  let n' = natVal proxy
  in FinitePolynom . fromIndexed . tidyIndexed .
     map (\(p, a) -> (p `mod` n', a)) $ toIndexed xs

freePolynom :: FinitePolynom field -> Polynom field
freePolynom (FinitePolynom ls) = Polynom ls

instance Show FinitePolynom where
  show = show . freePolynom

instance (KnownNat n, Num field, Eq field) => Num (FinitePolynom field) where
  a + b = truncatePolynom (Proxy :: Proxy n) $ freePolynom a + freePolynom b
  a * b = truncatePolynom (Proxy :: Proxy n) $ freePolynom a * freePolynom b
  negate (FinitePolynom ls) = FinitePolynom $ map negate ls
  abs (FinitePolynom ls) = FinitePolynom $ map abs ls
  signum (FinitePolynom ls) = FinitePolynom $ map signum ls
