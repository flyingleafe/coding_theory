#!/bin/bash

file=$1

pdflatex $file

rm $file.{bib,aux,log,bbl,bcf,blg,run.xml,toc,tct}
